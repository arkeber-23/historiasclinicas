package modelo;

import domain.Persona;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static modelo.Conexion.*;

public class PersonaDao {

    private static final String SQL_SELECT = "SELECT * FROM historiasClinicas";
    private static final String SQL_INSERT = "INSERT INTO historiasClinicas(nombre,apellidos,numero_historia, anio_nacimiento) VALUES (?,?, ?, ?)";
    private static final String SQL_DELETE = "DELETE FROM historiasClinicas WHERE id = ?";
    private static final String SQL_UPDATE = "UPDATE historiasClinicas SET nombre = ?, apellidos = ?, numero_historia = ?,anio_nacimiento = ? WHERE id = ?";

    public List<Persona> seleccionar() {

        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Persona persona = null;
        List<Persona> personas = new ArrayList<>();

        try {
            conn = getConnection();
            stmt = conn.prepareStatement(SQL_SELECT);
            rs = stmt.executeQuery();
            while (rs.next()) {
                int idPersona = rs.getInt("id");
                String nombre = rs.getString("nombre");
                String apellidos = rs.getString("apellidos");
                String numeroHistoria = rs.getString("numero_historia");
                String anioNacimiento = rs.getString("anio_nacimiento");
                persona = new Persona(idPersona, nombre, apellidos, numeroHistoria, anioNacimiento);
                personas.add(persona);
            }
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            close(rs);
            close(stmt);
            close(conn);
        }
        return personas;
    }

    public int insertarNuevo(Persona persona) {
        Connection conn = null;
        PreparedStatement stmt = null;
        int resultado = 0;
        try {
            conn = getConnection();
            stmt = conn.prepareStatement(SQL_INSERT);
            stmt.setString(1, persona.getNombre());
            stmt.setString(2, persona.getApellidos());
            stmt.setString(3, persona.getNumeroHistoria());
            stmt.setString(4, persona.getFechaNacimiento());
            resultado = stmt.executeUpdate();

        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            close(stmt);
            close(conn);
        }
        return resultado;

    }

    public int actualizar(Persona persona) {
        Connection conn = null;
        PreparedStatement stmt = null;
        int resultado = 0;
        try {
            conn = getConnection();
            stmt = conn.prepareStatement(SQL_UPDATE);
            stmt.setString(1, persona.getNombre());
            stmt.setString(2, persona.getApellidos());
            stmt.setString(3, persona.getNumeroHistoria());
            stmt.setString(4, persona.getFechaNacimiento());
            stmt.setInt(5, persona.getIdPersona());
            resultado = stmt.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            close(stmt);
            close(conn);
        }
        return resultado;

    }
    
       public int eliminar(Persona persona) {
        Connection conn = null;
        PreparedStatement stmt = null;
        int resultado = 0;
        try {
            conn = getConnection();
            stmt = conn.prepareStatement(SQL_DELETE);
            stmt.setInt(1, persona.getIdPersona());
            resultado = stmt.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            close(stmt);
            close(conn);
        }
        return resultado;

    }

}
