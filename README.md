
# HISTORIAS CLINICAS
Es un proyecto **(DEMO)** que he desarrollado hace dos años, espero le sirve de ayuda!

### CONFIGURACIÓN
1. Importar la libreria de ***SQLITE3*** que se encuentra en la carpeta libreria
2. Descargar ***JCALENDAR*** 
- http://www.java2s.com/Code/JarDownload/jcalendar/jcalendar-1.4.jar.zip
3. Administrador de base de datos para SQLITE3
- https://sqlitebrowser.org/dl/
